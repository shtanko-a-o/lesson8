package exceptions;

public class DeficiencyCashException extends RuntimeException {
    public DeficiencyCashException(String message) {
        super(message);
    }
}
