package exceptions;

public class MultiplicitySumException extends RuntimeException {
    public MultiplicitySumException(String message) {
        super(message);
    }
}
