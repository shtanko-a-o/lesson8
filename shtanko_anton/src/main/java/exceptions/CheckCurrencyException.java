package exceptions;

public class CheckCurrencyException extends RuntimeException {
    public CheckCurrencyException(String message) {
        super(message);
    }
}
