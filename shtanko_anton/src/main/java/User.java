import java.util.Objects;

public class User {
    private final String firstName;
    private final String lastName;
    private final String middleName;
    private final String numberAccount;
    private int amountCash;

    public User(String lastName, String firstName, String middleName, String numberAccount, int amountCash) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.numberAccount = numberAccount;
        this.amountCash = amountCash;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getNumberAccount() {
        return numberAccount;
    }

    public int getAmountCash() {
        return amountCash;
    }

    public void setAmountCash(int amountCash) {
        this.amountCash = amountCash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return amountCash == user.amountCash && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(middleName, user.middleName) && Objects.equals(numberAccount, user.numberAccount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, middleName, numberAccount, amountCash);
    }
}
