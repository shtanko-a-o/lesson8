public interface Terminal {
    void transferBetweenYourAccount();

    boolean entryPinCode();

    void transferBetweenUserAccounts();
}
