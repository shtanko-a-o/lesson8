import exceptions.CheckCurrencyException;

public class FrodMonitor {
    private final Currency currency;

    public FrodMonitor(Currency currency) {
        this.currency = currency;
    }

    public void checkCurrency(Currency currencyTransfer) throws CheckCurrencyException {
        if (!currency.equals(currencyTransfer)) {
            throw new CheckCurrencyException("Выбранная валюта не соответствует валюте перевода!");
        }
    }

    public Currency getCurrency() {
        return currency;
    }
}
