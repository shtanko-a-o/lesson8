public enum Currency {
    EUR("Евро"),
    USD("Доллар"),
    RUB("Российский рубль");

    public final String russianTitle;

    Currency(String russianTitle) {
        this.russianTitle = russianTitle;
    }


}
