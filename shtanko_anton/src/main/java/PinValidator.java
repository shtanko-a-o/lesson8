import exceptions.AccountIsLockedException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class PinValidator {
    private final String pin;
    private final LocalDateTime dateTime;
    private final User user;

    public PinValidator(String pin, LocalDateTime dateTime, User user) {
        this.pin = pin;
        this.dateTime = dateTime;
        this.user = user;
    }

    public String getPin() {
        return pin;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public User getUser() {
        return user;
    }

    public boolean verificationPinCode() throws AccountIsLockedException {
        int count = 3;
        boolean isExit;
        System.out.println("Введите пин-код:");
        Scanner scanner = new Scanner(System.in);
        while (!pin.equals(scanner.nextLine())) {
            count--;
            if (count == 0) {
                throw new AccountIsLockedException("Аккаунт " + user.getNumberAccount() + " заблокирован до " +
                        (dateTime.plusSeconds(5).format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"))));
            }
            System.out.println("Неверный пин-код, попробуйте снова");
        }
        isExit = true;
        System.out.println("Доброго времени суток, " + user.getFirstName() + "!");
        return isExit;
    }
}
