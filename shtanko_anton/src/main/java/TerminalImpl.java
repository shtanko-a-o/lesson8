import exceptions.AccountIsLockedException;
import exceptions.CheckCurrencyException;
import exceptions.DeficiencyCashException;
import exceptions.MultiplicitySumException;

public class TerminalImpl implements Terminal {

    private final TerminalServer server;
    private final PinValidator pinValidator;
    private final FrodMonitor frodMonitor;


    public TerminalImpl(TerminalServer server, PinValidator pinValidator, FrodMonitor frodMonitor) {
        this.server = server;
        this.pinValidator = pinValidator;
        this.frodMonitor = frodMonitor;
    }

    /**
     * перевод между своими счетами
     */
    @Override
    public void transferBetweenYourAccount() {
        try {
            server.verificationSumTransfer(frodMonitor);
        } catch (DeficiencyCashException | MultiplicitySumException | CheckCurrencyException d) {
            System.out.println(d.getMessage());
        }
    }

    /**
     * перевод между счетами 2-х пользователей
     */

    @Override
    public void transferBetweenUserAccounts() {
        try {
            server.verificationSumTransfer(frodMonitor);
        } catch (DeficiencyCashException | MultiplicitySumException | CheckCurrencyException d) {
            System.out.println(d.getMessage());
        }
    }

    /**
     * проверка пин-кода
     */
    @Override
    public boolean entryPinCode() {
        try {
            return pinValidator.verificationPinCode();
        } catch (AccountIsLockedException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
}
