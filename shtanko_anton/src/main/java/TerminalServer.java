import exceptions.CheckCurrencyException;
import exceptions.DeficiencyCashException;
import exceptions.MultiplicitySumException;

import java.util.Scanner;

public class TerminalServer {
    private final User user;

    public TerminalServer(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void verificationSumTransfer(FrodMonitor frodMonitor) throws DeficiencyCashException, MultiplicitySumException, CheckCurrencyException {
        System.out.println("Введите номер счета, на который хотите перевести деньги:");
        Scanner scanner = new Scanner(System.in);
        String numberAccount = String.valueOf(scanner.nextBigDecimal());
        System.out.println("Введите сумму перевода:");
        int sumTransfer = scanner.nextInt();
        if (user.getAmountCash() >= sumTransfer && sumTransfer % 100 == 0) {
            Currency currency = Currency.EUR;
            frodMonitor.checkCurrency(currency);
            user.setAmountCash(user.getAmountCash() - sumTransfer);
            System.out.println("Перевод со счета " + user.getNumberAccount() + " суммой " + sumTransfer + " " +
                    currency.russianTitle + " на счет " + numberAccount + " выполнен успешно. Остаток на счете " +
                    user.getAmountCash() + " " + currency.russianTitle);
        } else if (user.getAmountCash() < sumTransfer) {
            throw new DeficiencyCashException("Сумма перевода " + sumTransfer + " превышает доступный остаток " +
                    user.getAmountCash());
        } else if (sumTransfer % 100 != 0) {
            throw new MultiplicitySumException("Сумма перевода должна быть кратна 100!");
        }
    }

}
