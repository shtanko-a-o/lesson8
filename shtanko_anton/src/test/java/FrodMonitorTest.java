import exceptions.CheckCurrencyException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FrodMonitorTest {

    private FrodMonitor frodMonitor;

    @BeforeClass
    public void createNewFrodMonitor() {
        frodMonitor = new FrodMonitor(Currency.EUR);
    }

    @Test
    public void currencyShouldBeEUR() {
        Assert.assertEquals(Currency.EUR, frodMonitor.getCurrency());
    }

    @Test(expectedExceptions = CheckCurrencyException.class, description = "Failed test")
    public void currencyShouldThrowCheckCurrencyException() throws CheckCurrencyException {
        frodMonitor.checkCurrency(Currency.RUB);
    }

    @DataProvider(name = "currencyData")
    public Object[][] checkCurrencyShouldSuccessData() {
        return new Object[][]{
                {frodMonitor.getCurrency()}
        };
    }

    @Test(dataProvider = "currencyData")
    public void checkCurrencyShouldSuccess(Currency currency) {
        frodMonitor.checkCurrency(currency);
    }
}
