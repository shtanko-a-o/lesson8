import exceptions.AccountIsLockedException;

import java.time.LocalDateTime;

public class Test {
    public static void main(String[] args) throws AccountIsLockedException {
        User user = new User("Штанько", "Антон", "Олегович", "1234", 5000);
        PinValidator pinValidator = new PinValidator("5565", LocalDateTime.now(), user);
        TerminalImpl terminal = new TerminalImpl(new TerminalServer(user), pinValidator, new FrodMonitor(Currency.EUR));
        if (terminal.entryPinCode()) {
            terminal.transferBetweenYourAccount();
        }
    }
}
