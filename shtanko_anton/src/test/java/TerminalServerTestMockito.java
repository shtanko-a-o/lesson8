import exceptions.DeficiencyCashException;
import exceptions.MultiplicitySumException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TerminalServerTestMockito {

    @Mock
    private FrodMonitor frodMonitor;

    @Mock
    private User user;

    @InjectMocks
    private TerminalServer server;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        when(user.getAmountCash()).thenReturn(5000);
    }

    @Test
    public void verificationSumTransferSuccess() {
        File file = new File("C:\\ProjectsIdea\\SberTraineeShip\\lesson8\\test.txt");
        String numberAccount = "";
        int sumTransfer = 0;
        try (BufferedReader buff = new BufferedReader(new FileReader(file))) {
            numberAccount = buff.readLine();
            sumTransfer = Integer.parseInt(buff.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayInputStream in = new ByteArrayInputStream((numberAccount + "\n" + sumTransfer).getBytes());
        System.setIn(in);
        doNothing().when(frodMonitor).checkCurrency(any());
        server.verificationSumTransfer(frodMonitor);
        Assert.assertTrue(sumTransfer % 100 == 0);
        Assert.assertTrue(server.getUser().getAmountCash() > sumTransfer);
    }

    @Test(expectedExceptions = DeficiencyCashException.class)
    public void verificationSumTransferShouldThrowDeficiencyCashException() throws DeficiencyCashException {
        File file = new File("C:\\ProjectsIdea\\SberTraineeShip\\lesson8\\test.txt");
        String numberAccount = "";
        int sumTransfer = 0;
        try (BufferedReader buff = new BufferedReader(new FileReader(file))) {
            numberAccount = buff.readLine();
            sumTransfer = Integer.parseInt(buff.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayInputStream in = new ByteArrayInputStream((numberAccount + "\n" + sumTransfer).getBytes());
        System.setIn(in);
        when(user.getAmountCash() < sumTransfer).thenThrow(new DeficiencyCashException("Сумма перевода превышает доступный остаток"));
        server.verificationSumTransfer(frodMonitor);
    }

    @Test(expectedExceptions = MultiplicitySumException.class)
    public void verificationSumTransferShouldThrowMultiplicitySumException() throws MultiplicitySumException {
        File file = new File("C:\\ProjectsIdea\\SberTraineeShip\\lesson8\\test.txt");
        String numberAccount = "";
        int sumTransfer = 0;
        try (BufferedReader buff = new BufferedReader(new FileReader(file))) {
            numberAccount = buff.readLine();
            sumTransfer = Integer.parseInt(buff.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayInputStream in = new ByteArrayInputStream((numberAccount + "\n" + sumTransfer).getBytes());
        System.setIn(in);
        when(sumTransfer % 100 != 0).thenThrow(new MultiplicitySumException("Сумма перевода должна быть кратна 100!"));
        server.verificationSumTransfer(frodMonitor);
    }


}
