import exceptions.CheckCurrencyException;
import exceptions.DeficiencyCashException;
import exceptions.MultiplicitySumException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;


public class TerminalImplTestMockito {

    @Mock
    private TerminalServer server;

    @Mock
    private FrodMonitor frodMonitor;

    @InjectMocks
    private TerminalImpl terminal;


    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void transferBetweenYourAccountShouldBeSuccess() {
        doNothing().when(server).verificationSumTransfer(frodMonitor);
        terminal.transferBetweenUserAccounts();
    }

    @Test(expectedExceptions = MultiplicitySumException.class)
    public void transferBetweenYourAccountShouldThrowMultiplicitySumException() throws MultiplicitySumException {
        doThrow(new MultiplicitySumException("Сумма перевода должна быть кратна 100!")).when(server).verificationSumTransfer(any());
        terminal.transferBetweenYourAccount();
    }

    @Test(expectedExceptions = DeficiencyCashException.class)
    public void transferBetweenYourAccountShouldThrowDeficiencyCashException() throws DeficiencyCashException {
        doThrow(new DeficiencyCashException("Сумма перевода превышает доступный остаток.")).when(server).verificationSumTransfer(any());
        terminal.transferBetweenYourAccount();
    }

    @Test(expectedExceptions = CheckCurrencyException.class)
    public void transferBetweenYourAccountShouldThrowCheckCurrencyException() throws CheckCurrencyException {
        doThrow(new CheckCurrencyException("Выбранная валюта не соответствует валюте перевода!")).when(server).verificationSumTransfer(any());
        terminal.transferBetweenYourAccount();
    }
}
