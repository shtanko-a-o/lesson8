import exceptions.AccountIsLockedException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;

public class PinValidatorTest {

    private PinValidator pinValidator;

    @BeforeClass
    public void createNewPinValidator() {
        pinValidator = new PinValidator("5565", LocalDateTime.now(), new User("Штанько",
                "Антон", "Олегович", "88888888", 5000));
    }

    @Test
    public void pinShouldBeCorrect() {
        Assert.assertEquals("5565", pinValidator.getPin());
    }

    @Test(expectedExceptions = AccountIsLockedException.class, description = "Failed test")
    public void pinValidatorShouldThrowAccountIsLockedException() throws AccountIsLockedException {
        ByteArrayInputStream in = new ByteArrayInputStream("8888\n6666\n4444".getBytes());
        InputStream inputStream = System.in;
        System.setIn(in);
        pinValidator.verificationPinCode();
        System.setIn(inputStream);
    }

    @Test
    public void pinValidatorShouldSuccess() {
        ByteArrayInputStream in = new ByteArrayInputStream("5565\n5565\n5565".getBytes());
        InputStream inputStream = System.in;
        System.setIn(in);
        pinValidator.verificationPinCode();
        System.setIn(inputStream);
    }
}
